from django.urls import path, re_path
from django.conf.urls import url
from jadwalku import views

app_name = "jadwalku"

urlpatterns = [
    path('', views.jadwal, name="jadwal"),
    path('tambah-jadwal/', views.tambahJadwal, name = "tambah-jadwal"),
    url(r'^delete/(?P<delete_id>[0-9])$', views.delete, name="delete"),
]