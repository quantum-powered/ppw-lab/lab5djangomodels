from django.db import models

# Create your models here.
class Jadwal(models.Model):
    nama_kegiatan = models.CharField(max_length=20, unique = True) 
    # CharField untuk tipe karakter (contohnya string)
    tanggal = models.DateField()
    tempat = models.CharField(max_length=50)
    kategori = models.ForeignKey('Kategori', on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.nama_kegiatan

class Kategori(models.Model):
    nama_kategori = models.CharField(max_length=50, unique = True)

    def __str__(self):
        return self.nama_kategori