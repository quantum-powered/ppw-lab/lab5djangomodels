from django.contrib import admin
from jadwalku.models import Jadwal, Kategori

# Register your models here.
admin.site.register(Jadwal)
admin.site.register(Kategori)
