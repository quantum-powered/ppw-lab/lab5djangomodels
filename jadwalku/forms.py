from django import forms
from jadwalku.models import Jadwal, Kategori

class FormKegiatan(forms.ModelForm):
    class Meta():
        model = Jadwal
        fields = "__all__"
        widgets = {
            'nama_kegiatan' : forms.TextInput(
                attrs = {
                    'class' : 'form-control',
                }
            ), 
            'tanggal' : forms.DateInput(
                attrs = {
                    'class' : 'form-control'
                }
            ),
            'tempat' : forms.TextInput(
                attrs = {
                    'class' : 'form-control'
                }
            )
        }