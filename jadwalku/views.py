from django.shortcuts import render, redirect
from jadwalku.models import Jadwal, Kategori
from jadwalku.forms import FormKegiatan

# Create your views here.
def jadwal(request):
    kegiatan_list = Jadwal.objects.order_by('tanggal')
    form = FormKegiatan()
    query = {
        'form' : form,
        'halo' : 'andhika',
        'kegiatan_list' : kegiatan_list,
    }

    if request.method == 'POST':
        form = FormKegiatan(request.POST)

        if form.is_valid():
            form.save(commit = True)
            print("VALID")
            print("Name: "+ form.cleaned_data['nama_kegiatan'])
            print("Tanggal: "+ str(form.cleaned_data['tanggal']))
            print("Tempat: "+ form.cleaned_data['tempat'])



    return render(request, 'jadwalku/jadwal.html', context=query)

def tambahJadwal(request):
    form = FormKegiatan()
    query = {
        'form' : form,
        'halo' : 'andhika',
    }

    if request.method == 'POST':
        form = FormKegiatan(request.POST)

        if form.is_valid():
            form.save(commit = True)
            print("VALID")
            print("Name: "+ form.cleaned_data['nama_kegiatan'])
            print("Tanggal: "+ str(form.cleaned_data['tanggal']))
            print("Tempat: "+ form.cleaned_data['tempat'])



    return render(request, 'jadwalku/buat-jadwal.html', context=query)

def delete(request, delete_id):
    Jadwal.objects.filter(id=delete_id).delete()
    return redirect('jadwalku:jadwal')
