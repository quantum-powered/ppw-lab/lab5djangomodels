from django.shortcuts import render


# Create your views here.
def index(request):
    return render(request, "frontpage/index.html", {})

def contact_me(request):
    return render(request, "frontpage/contact-me.html", {})

def about_me(request):
    return render(request, "frontpage/about-me.html", {})

def projects(request):
    return render(request, "frontpage/projects.html", {})