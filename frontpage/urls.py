from django.urls import path, include
from frontpage import views

app_name = "frontpage"

urlpatterns = [
    path('', views.index, name="index"),
    path('contact-me/', views.contact_me, name="contact-me"),
    path('about-me/', views.about_me, name="about-me"),
    path('projects/', views.projects, name="projects")
]